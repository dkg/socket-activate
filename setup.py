#!/usr/bin/python3

""" Set up socket_activate. """

import re
import setuptools
import sys


RE_VERSION = re.compile(r'''^
    version \s+
    (?P<version>
           (?: 0 | [1-9][0-9]* )        # major
        \. (?: 0 | [1-9][0-9]* )        # minor
    )
    \s+
''', re.X)


def get_version():
    """ Get the version string from the NEWS file. """
    with open('NEWS') as changelog:
        for line in changelog.readlines():
            match = RE_VERSION.match(line)
            if match:
                return match.group('version')

    sys.exit('Could not find a version line in the NEWS file')


def get_long_description():
    """ Get the package long description from the README file. """
    with open('README.md') as readme:
        return readme.read()


setuptools.setup(
    name='socket-activate',
    version=get_version(),

    description='Open sockets and start a socket-activated service',
    long_description=get_long_description(),
    long_description_content_type='text/markdown',

    author='Daniel Kahn Gillmor',
    author_email='dkg@fifthhorseman.net',
    url='https://gitlab.com/dkg/socket-activate',

    packages=['socket_activate'],

    license='GPL-3+',
    classifiers=[
        'Development Status :: 4 - Beta',

        'Intended Audience :: System Administrators',

        'License :: DFSG approved',
        'License :: Freely Distributable',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',

        'Operating System :: POSIX',
        'Operating System :: Unix',

        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3 :: Only',

        'Topic :: System :: Boot',
        'Topic :: System :: Systems Administration',
        'Topic :: Utilities',
    ],

    entry_points={
        'console_scripts': [
            'socket-activate=socket_activate.__main__:main',
        ],
    },

    zip_safe=True,
)
