#!/usr/bin/make -f

OBJECTS=socket-activate.1

all: $(OBJECTS)

socket-activate.1: socket-activate.md
	pandoc -s -t man -o $@ $<

clean:
	rm -f $(OBJECTS) \
		tests/cmdline-errors.txt tests/cmdline-output.txt \
		tests/expected tests/produced
	find tests/ -type s -name 'sock*' -delete
	rm -rf \
		.mypy_cache \
		.tox \
		build \
		dist \
		socket_activate.egg-info \
		socket_activate/__pycache__/ \
		unit_tests/__pycache__/

check: check-python

check-python:
	SOCKET_ACTIVATE=$(CURDIR)/scripts/socket-activate.sh ./tests/cmdline
	SOCKET_ACTIVATE=$(CURDIR)/scripts/socket-activate.sh ./tests/basic
	SOCKET_ACTIVATE=$(CURDIR)/scripts/socket-activate.sh ./tests/unix
	SOCKET_ACTIVATE=$(CURDIR)/scripts/socket-activate.sh ./tests/inet

VERSION ?= $(shell awk '/^version/{ print $$2 }' < NEWS | head -n1)

release:
	git tag -s socket-activate_$(VERSION) -m 'Tagging socket-activate version $(VERSION)' master

.PHONY: clean all check check-python release
